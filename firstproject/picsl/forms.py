from django import forms
from .models import Image

class ImageForm(forms.ModelForm):
    # your_name = forms.CharField(label='Your name', max_length=100)
    class Meta:
        model = Image
        fields = ("title", "image")
