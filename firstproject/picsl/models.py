from django.db import models
import exif
from datetime import datetime

# Create your models here.

class Images(models.Model):
    title = models.CharField(max_length=50)
    description = models.CharField(max_length=280)
    image = models.ImageField(upload_to = '%Y/')
    date_loaded = models.DateTimeField(auto_now_add=True)
    datetime_original = models.DateTimeField(null=True)

    def save(self, *args, **kwargs):
        """Override save to get exif data before saving"""

        imageExif = exif.Image(self.image)
        if (imageExif.datetime_original):
            do = imageExif.datetime_original
            self.datetime_original = datetime.strptime(do, '%Y:%m:%d %H:%M:%S')

        super(Images, self).save(*args, **kwargs)

    def __str__(self):
        return self.title
