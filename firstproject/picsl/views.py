# from .models import Question
from django.shortcuts import render
# from django.http import HttpResponse, HttpResponseRedirect
from .models import Images
# from django.urls import reverse

def index(request):
    """ display front page """
    all_images = Images.objects.all()
    context = {"images": all_images}
    return render(request, "picsl/index.html", context)


def upload(request):
    """Upload image form"""
    if (request.method) == "POST":
        image = Images(
            title = request.POST['title'],
            image = request.FILES['image'],
            description = request.POST['description'],
        )
        image.save()
        return render(request, "picsl/upload.html", {
            "image_loaded": True,
            "image_title": request.POST['title'],
            "image_description": request.POST['description'],
        })
    else:
        return render(request, "picsl/upload.html")
