# Image Upload

The image upload is using Pillow and Django's built in ImageField in the model.

In `settings.py` the root parameters are set up as follows.

```python
MEDIA_URL = 'images/' # serve media from
MEDIA_ROOT = BASE_DIR / 'picsl/static/picsl/images/' # storing media in
```

These are included in the root `urls.py` file to allow the django dev server to serve the media files. Need to figure out how to set this up for a live server.

```python
# For django's development server.
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL,
        document_root=settings.MEDIA_ROOT)
```

Now the model can be set up to save images. I'm using the `%Y` date parameter to manage the folder size.

```python
image = models.ImageField(upload_to = '%Y/')
```

## Exif and Meta Data

I can use the `exif` package to grab any exif data from the image, and add that to the model before upload. This is all handled in the Models file.

Data I want:

- title (non null) - Title of Picture. Up to 50 chars. Manually entered by user (defaults to filename)
- description (null) - short description of image. Up to 280 characters. Manually entered by user
- datetime_uploaded (non null) - default to current date from system
- datetime_original (null) - taken from the exif data datetime_original

Exif dates (or at least the examples I've seen) are in the format: 2022:06:04 14:57:54, so need to be convert to Python datetime.datetime instances. e.g.

```python
datetime.strptime(exif_date, '%Y:%m:%d %H:%M:%S')
```

## View & Template

The `upload` view is a simple form that lets user enter Title, Description and Upload and Image from their files.

If data is POSTed to the view, then is supplies the data to the model to save and then displays results to the user. Otherwise the plain form is shown.