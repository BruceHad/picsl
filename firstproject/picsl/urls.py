from django.urls import path 
from . import views 

app_name = 'picsl' 

urlpatterns = [ 
	path('', views.index, name='index'),
	# path('<int:question_id>/', views.question, name='question'),
	path('upload/', views.upload, name='upload'),
]