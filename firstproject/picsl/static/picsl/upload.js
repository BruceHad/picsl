const imagePreview = document.getElementById('image-preview');
const imageSelect = document.getElementById('image-select');
const titleInput  = document.getElementById('title-input');

imageSelect.addEventListener('change', function(){
    getImageData();
});

function getImageData(){
    const imageFile = imageSelect.files[0];
    console.log(imageFile);
    if(imageFile){
        titleInput.value = imageFile.name;
        const fileReader = new FileReader();
        fileReader.readAsDataURL(imageFile);
        fileReader.addEventListener('load', function(){
            imagePreview.style.display = 'block';
            imagePreview.innerHTML = '<img src="' +
                this.result +
                '" style="max-height: 500px;"/>';
        });
    }
}