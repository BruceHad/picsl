from .models import Question
from django.shortcuts import render
from django.http import HttpResponse

def index(request):
    latest_question_list = Question.objects.order_by('-pub_date')[:5]
    context = {'latest_question_list': latest_question_list}
    return render(request, 'picsl/index.html', context)

def upload(request):
    return render(request, 'picsl/upload.html')
